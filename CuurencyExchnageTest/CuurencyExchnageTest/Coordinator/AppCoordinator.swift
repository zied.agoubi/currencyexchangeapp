//
//  AppCoordinator.swift
//  CuurencyExchnageTest
//
//  Created by MacBook Pro Catalina on 07/03/2020.
//  Copyright © 2020 MacBook Pro Catalina. All rights reserved.
//

import Foundation
import UIKit

class AppCoordinator: Coordinator {
    
    weak var coordinatorDelegate: CoordinatorDelegate?
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
        
    init(navigationController: UINavigationController, delegate: CoordinatorDelegate?) {
        self.navigationController = navigationController
        self.coordinatorDelegate = delegate
    }
    
    func start() {
        showLogin()
    }
    
    func showLogin() {
        let loginCoordinator = LoginCoordinator(navigationController: navigationController, delegate: self)
        loginCoordinator.start()
        self.childCoordinators.append(loginCoordinator)
    }
    
    func showHome() {
        let homeCoordinator = HomeCoordinator(navigationController: navigationController, delegate: self)
        homeCoordinator.start()
        self.childCoordinators.append(homeCoordinator)
    }
    
}

extension AppCoordinator: LoginCoordinatorDelegate {
    
    func showHome(_ coordinator: LoginCoordinator) {
        showHome()
    }

}
