//
//  Coordinator.swift
//  CuurencyExchnageTest
//
//  Created by MacBook Pro Catalina on 07/03/2020.
//  Copyright © 2020 MacBook Pro Catalina. All rights reserved.
//

import UIKit

protocol CoordinatorDelegate: class {
    func coordinatorDidExit(_ coordinator: Coordinator)
}

protocol Coordinator: CoordinatorDelegate {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    var coordinatorDelegate: CoordinatorDelegate? { get set }
    
    func start()
}

extension Coordinator {
    
    func coordinatorDidExit(_ coordinator: Coordinator) {
        guard let index = self.childCoordinators.index(where: { $0 === coordinator }) else { return }
        self.childCoordinators.remove(at: index)
    }
}
