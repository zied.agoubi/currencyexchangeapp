//
//  HistoryCoordinator.swift
//  CuurencyExchnageTest
//
//  Created by MacBook Pro Catalina on 08/03/2020.
//  Copyright © 2020 MacBook Pro Catalina. All rights reserved.
//

import UIKit

class HistoryCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    weak var coordinatorDelegate: CoordinatorDelegate?
    
    init(navigationController: UINavigationController, delegate: CoordinatorDelegate?) {
        self.navigationController = navigationController
        self.coordinatorDelegate = delegate
    }
    
    func start() {
        let viewModel = HistoryViewModel()
        let history = HistoryViewController(nibName: nil, bundle: nil, viewModel: viewModel)
        self.navigationController.pushViewController(history, animated: true)
    }
    
}
