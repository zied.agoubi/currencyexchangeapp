//
//  HomeCoordinator.swift
//  CuurencyExchnageTest
//
//  Created by MacBook Pro Catalina on 08/03/2020.
//  Copyright © 2020 MacBook Pro Catalina. All rights reserved.
//

import UIKit

class HomeCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    weak var coordinatorDelegate: CoordinatorDelegate?
    
    init(navigationController: UINavigationController, delegate: CoordinatorDelegate?) {
        self.navigationController = navigationController
        self.coordinatorDelegate = delegate
    }
    
    func start() {
        let viewModel = HomeViewModel()
        let home = HomeViewController(nibName: nil, bundle: nil, viewModel: viewModel)
        home.delegate = self
        self.navigationController.pushViewController(home, animated: true)
    }
    
    func showHistory() {
        let historyCoordinator = HistoryCoordinator(navigationController: navigationController, delegate: self)
        historyCoordinator.start()
        self.childCoordinators.append(historyCoordinator)
    }
    
    
}

extension HomeCoordinator: HomeViewControllerDelegate {
    
    func didTapHistory(homevc: HomeViewController) {
        showHistory()
    }
    
}
