//
//  LoginCoordinator.swift
//  CuurencyExchnageTest
//
//  Created by MacBook Pro Catalina on 07/03/2020.
//  Copyright © 2020 MacBook Pro Catalina. All rights reserved.
//

import UIKit

protocol LoginCoordinatorDelegate: CoordinatorDelegate {
    func showHome(_ coordinator: LoginCoordinator)
}

class LoginCoordinator: Coordinator {

    weak var delegate: LoginCoordinatorDelegate?
    var coordinatorDelegate: CoordinatorDelegate?
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    
    func start() {
        self.showLogin()
    }
    
    init(navigationController: UINavigationController, delegate: LoginCoordinatorDelegate) {
        self.navigationController = navigationController
        self.coordinatorDelegate = delegate
        self.delegate = delegate
    }
    
    func showLogin() {
        let viewModel = LoginViewModel()
        let loginViewController = LoginViewController(nibName: nil, bundle: nil, viewModel: viewModel)
        loginViewController.delegate = self
        loginViewController.statusBar(light: true)
        self.navigationController.setViewControllers([loginViewController], animated: true)
    }

}

extension LoginCoordinator: LoginViewControllerDelegate {
    
    func didSuccesfullLogin() {
        delegate?.showHome(self)
    }
}
