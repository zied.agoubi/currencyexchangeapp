//
//  CEButton.swift
//  CuurencyExchnageTest
//
//  Created by MacBook Pro Catalina on 07/03/2020.
//  Copyright © 2020 MacBook Pro Catalina. All rights reserved.
//

import UIKit

@IBDesignable
class CEButton: UIButton {
    
    //MARK: - Properties
    var buttonHasBorders:Bool = false
    var buttonHasUperCasedTitle:Bool = false
    
    //MARK: -Initializers
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.commonInit()
    }
  
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    //MARK: - Functions
    
    ///This functions will take care of setting the font of the title, the alignment of the title and the corner radius
    func commonInit() {
    
        self.titleLabel?.apply(typography: .huge(weight: .Roboto_Black), with: .white)
        self.titleLabel?.textAlignment = .center
        self.layer.cornerRadius = 4
        self.clipsToBounds = true
    }
    
    ///This functions will take care of the custom configuration of the button
    func configureButton(title: String, backgroundColor: UIColor, titleColor: UIColor, borderColor: UIColor) {
        
        self.backgroundColor = backgroundColor
        setTitleColor(titleColor, for: .normal)
        self.setTitle(title, for: .normal)
        
        if buttonHasBorders == true {
            giveBorderToButton(borderColor: borderColor)
        }
        
        if buttonHasUperCasedTitle == true {
            makeTitleUperCased()
        }
    }
    
    func giveBorderToButton(borderColor: UIColor) {
        self.layer.borderWidth = 2
        self.layer.borderColor = borderColor.cgColor
    }
    
    func makeTitleUperCased() {
        self.setTitle(self.title(for: .normal)?.uppercased(), for: .normal)
    }
    
}
