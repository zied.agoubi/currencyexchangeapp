//
//  CETextfield.swift
//  CuurencyExchnageTest
//
//  Created by MacBook Pro Catalina on 07/03/2020.
//  Copyright © 2020 MacBook Pro Catalina. All rights reserved.
//

import Foundation
import UIKit

protocol CRFTextfieldDelegate: class {
    func clickOnRightButton(textField: CETextfield, _ sender: UIButton)
}

@IBDesignable
class CETextfield: UITextField {
    
    //MARK: - Constants
    
    struct Metrics {

    }
    
    //MARK: - Properties
    
    let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    
    var button: UIButton = {
        let button = UIButton(type: .custom)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        button.addTarget(self, action: #selector(rightButtonClicked), for: .touchUpInside)
        return button
    }()
    //MARK: - Delegates
    
    weak var crfDelegate: CRFTextfieldDelegate?
    
    //MARK: - Overrided Functions

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
      return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
      return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
      return bounds.inset(by: padding)
    }
    
    //MARK: - Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    //MARK: - Functions
    
    func commonInit() {
        
        self.backgroundColor = .clear
        self.textAlignment = .left
        self.font = .currencyExchangeFont(ofSize: 24, weight: .Roboto_Regular)
        self.borderStyle = .none
        self.layer.cornerRadius = 4
        self.clipsToBounds = true
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(origin: CGPoint(x: 0, y: self.frame.height - 1), size: CGSize(width: self.frame.width, height:  1))
        bottomLine.backgroundColor = UIColor.mediumGray.cgColor
        self.layer.addSublayer(bottomLine)
        
        
    }
    
    func configure(placeholder: String, placeholderColor: UIColor) {
        self.attributedPlaceholder = NSAttributedString(string: placeholder,attributes: [NSAttributedString.Key.foregroundColor: placeholderColor])
    }
    
    func addRightButtonToTextField(width: CGFloat, height: CGFloat, image: UIImage) {
        
        button.setImage(image, for: .normal)
        button.frame = CGRect(x: CGFloat(self.frame.size.width - 25), y: CGFloat(5), width: width, height: height)
        self.rightView = button
        self.rightViewMode = .always
    }
    
    @objc func rightButtonClicked() {
        crfDelegate?.clickOnRightButton(textField: self, button)
    }
    
}
