//
//  ColorExtensions.swift
//  CuurencyExchnageTest
//
//  Created by MacBook Pro Catalina on 07/03/2020.
//  Copyright © 2020 MacBook Pro Catalina. All rights reserved.
//

import Foundation
import UIKit

fileprivate class CurrencyExchangeColors { }

extension UIColor {
    
    /// A color object whose hex value is #FFD185 and whose alpha value is 1.0.
    public class var lightYellow: UIColor {
        if #available(iOS 11.0, *) {
            return UIColor(named: "light_yellow", in: Bundle(for: CurrencyExchangeColors.self), compatibleWith: nil)!
        } else {
            return #colorLiteral(red: 1, green: 0.8196078431, blue: 0.5215686275, alpha: 1)
        }
    }
    
    /// A color object whose hex value is #3C4858 and whose alpha value is 1.0.
    public class var darkGray: UIColor {
        if #available(iOS 11.0, *) {
            return UIColor(named: "dark_gray", in: Bundle(for: CurrencyExchangeColors.self), compatibleWith: nil)!
        } else {
            return #colorLiteral(red: 0.2352941176, green: 0.2823529412, blue: 0.3450980392, alpha: 1)
        }
    }
    
    /// A color object whose hex value is #8492A6 and whose alpha value is 1.0.
    public class var mediumGray: UIColor {
        if #available(iOS 11.0, *) {
            return UIColor(named: "medium_gray", in: Bundle(for: CurrencyExchangeColors.self), compatibleWith: nil)!
        } else {
            return #colorLiteral(red: 0.5176470588, green: 0.5725490196, blue: 0.6509803922, alpha: 1)
        }
    }
    
    /// A color object whose hex value is #C0CCDA and whose alpha value is 1.0.
    public class var lightGray: UIColor {
        if #available(iOS 11.0, *) {
            return UIColor(named: "light_gray", in: Bundle(for: CurrencyExchangeColors.self), compatibleWith: nil)!
        } else {
            return #colorLiteral(red: 0.7529411765, green: 0.8, blue: 0.8549019608, alpha: 1)
        }
    }
    
}
