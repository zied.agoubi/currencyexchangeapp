//
//  Constants.swift
//  CuurencyExchnageTest
//
//  Created by MacBook Pro Catalina on 08/03/2020.
//  Copyright © 2020 MacBook Pro Catalina. All rights reserved.
//

import Foundation

//MARK: - Completion handler

typealias CompletionHandler = (_ Success: Bool, _ data:Data?) -> ()

//MARK: - Header

var HEADER = [
    "Accept": "application/json"
]

//MARK: - Base Url

let BASE_URL = "https://api.exchangeratesapi.io/"


//MARK: - API Urls
 let ALL_CURRENCY = "latest"
