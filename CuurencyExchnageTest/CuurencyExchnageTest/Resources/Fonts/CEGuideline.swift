//
//  CEGuideline.swift
//  CuurencyExchnageTest
//
//  Created by MacBook Pro Catalina on 07/03/2020.
//  Copyright © 2020 MacBook Pro Catalina. All rights reserved.
//

import Foundation
import UIKit

///Every UI in this project supposed to conform to this guideline.
struct CEGuideline {
    
    struct FontSizes {
        static let hugeSize: CGFloat = 18
        static let bigSize: CGFloat = 16
        static let captionSize: CGFloat = 14
        static let littleSize: CGFloat = 12
        static let tinySize: CGFloat = 10
    }
    
    /// Typograph constraints.
    enum Typography {
        case huge(weight: UIFont.CurrencyExchangeWeight)
        case big(weight: UIFont.CurrencyExchangeWeight)
        case caption(weight: UIFont.CurrencyExchangeWeight)
        case little(weight: UIFont.CurrencyExchangeWeight)
        case tiny(weight: UIFont.CurrencyExchangeWeight)

        /// Typography fonts. Use these fonts across your project. You should never instantiate a custom font.
        /// Always use fonts from this Guideline.
        var font: UIFont {
            switch self {
            case .huge(let weight): return .currencyExchangeFont(ofSize: FontSizes.hugeSize, weight: weight)
            case .big(let weight): return .currencyExchangeFont(ofSize: FontSizes.bigSize, weight: weight)
            case .caption(let weight): return .currencyExchangeFont(ofSize: FontSizes.captionSize, weight: weight)
            case .little(let weight): return .currencyExchangeFont(ofSize: FontSizes.littleSize, weight: weight)
            case .tiny(let weight): return .currencyExchangeFont(ofSize: FontSizes.tinySize, weight: weight)
            }
        }

        /// Typography line spacing. Use these line spacing across your project. You should never create a new line spacing.
        /// Always use line spacing from this Guideline.
        var lineSpacing: CGFloat {
            switch self {
            case .huge(_): return 1
            case .big(_): return 1
            case .caption(_): return 1
            case .little(_): return 1
            case .tiny(_): return 1
            }
        }
    }

}
