//
//  UIFontExtensions.swift
//  CuurencyExchnageTest
//
//  Created by MacBook Pro Catalina on 07/03/2020.
//  Copyright © 2020 MacBook Pro Catalina. All rights reserved.
//

import Foundation
import UIKit

fileprivate class CurrencyExchangeFonts { }

extension UIFont {
    
    /// currency exchange weight
    ///
    /// - regular: Regular weight
    /// - semibold: Semibold weight
    enum CurrencyExchangeWeight: CaseIterable {
        
        case Roboto_Black
        case Roboto_BlackItalic
        case Roboto_Bold
        case Roboto_BoldItalic
        case Roboto_Italic
        case Roboto_Light
        case Roboto_LightItalic
        case Roboto_Medium
        case Roboto_MediumItalic
        case Roboto_Regular
        case Roboto_Thin
        case Roboto_ThinItalic
        
        // MARK: INTERNAL PROPERTIES
        
        var fontName: String {
            switch self {

            case .Roboto_Black: return "Roboto-Black"
            case .Roboto_BlackItalic: return "Roboto-BlackItalic"
            case .Roboto_Bold: return "Roboto-Bold"
            case .Roboto_BoldItalic: return "Roboto-BoldItalic"
            case .Roboto_Italic: return "Roboto-Italic"
            case .Roboto_Light: return "Roboto-Light"
            case .Roboto_LightItalic: return "Roboto-LightItalic"
            case .Roboto_Medium: return "Roboto-Medium"
            case .Roboto_MediumItalic: return "Roboto-MediumItalic"
            case .Roboto_Regular: return "Roboto-Regular"
            case .Roboto_Thin: return "Roboto-Thin"
            case .Roboto_ThinItalic: return "Roboto-ThinItalic"
            
            }
        }

        var asUIFontWeight: UIFont.Weight {
            switch self {
            
            case .Roboto_Black: return .regular
            case .Roboto_BlackItalic: return .regular
            case .Roboto_Bold: return .bold
            case .Roboto_BoldItalic: return .bold
            case .Roboto_Italic: return .regular
            case .Roboto_Light: return .regular
            case .Roboto_LightItalic: return .regular
            case .Roboto_Medium: return .medium
            case .Roboto_MediumItalic: return .medium
            case .Roboto_Regular: return .regular
            case .Roboto_Thin: return .thin
            case .Roboto_ThinItalic: return .thin
                
            }
        }
    }

    /// Static function that creates currency exchange Fonts
    ///
    /// - Parameters:
    /// - fontSize: Font size
    /// - weight: Font weight
    /// - Returns: Currency exchange Font with specified parameters
    class func currencyExchangeFont(ofSize fontSize: CGFloat, weight: UIFont.CurrencyExchangeWeight) -> UIFont {
        guard let currencyExchangeFont = UIFont(name: weight.fontName, size: fontSize) else {
            return UIFont.systemFont(ofSize: fontSize, weight: weight.asUIFontWeight)
        }

        return currencyExchangeFont
    }
    
}

