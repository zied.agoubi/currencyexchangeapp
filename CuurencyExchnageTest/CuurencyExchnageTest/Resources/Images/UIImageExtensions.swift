//
//  UIImageExtensions.swift
//  CuurencyExchnageTest
//
//  Created by MacBook Pro Catalina on 07/03/2020.
//  Copyright © 2020 MacBook Pro Catalina. All rights reserved.
//

import Foundation
import UIKit

fileprivate class CurrencyExchangeImages { }

extension UIImage {
    
    public class var icBack: UIImage {
        return UIImage(named: "ic_back", in: Bundle(for: CurrencyExchangeImages.self), compatibleWith: nil)!
    }
    
    public class var icFacebook: UIImage {
        return UIImage(named: "ic_facebook", in: Bundle(for: CurrencyExchangeImages.self), compatibleWith: nil)!
    }
    
    public class var icGoogle: UIImage {
        return UIImage(named: "ic_google", in: Bundle(for: CurrencyExchangeImages.self), compatibleWith: nil)!
    }
    
    public class var icTwitter: UIImage {
        return UIImage(named: "ic_twitter", in: Bundle(for: CurrencyExchangeImages.self), compatibleWith: nil)!
    }
    
    public class var icArrowDown: UIImage {
        return UIImage(named: "ic_arrow_down", in: Bundle(for: CurrencyExchangeImages.self), compatibleWith: nil)!
    }
    
}
