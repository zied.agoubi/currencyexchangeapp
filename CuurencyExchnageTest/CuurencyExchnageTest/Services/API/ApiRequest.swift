//
//  ApiRequest.swift
//  CuurencyExchnageTest
//
//  Created by MacBook Pro Catalina on 08/03/2020.
//  Copyright © 2020 MacBook Pro Catalina. All rights reserved.
//

import Foundation
import Alamofire

class ApiRequest {
    
    static let instance = ApiRequest()
    
    //MARK: - API Functions
    
    ///this function iis used to get list of currency
    
    func getCurrency(completion: @escaping CompletionHandler) {
        
        AF.request("\(BASE_URL)\(ALL_CURRENCY)", method: .get, parameters: nil).responseJSON(completionHandler: { (response) in
            if response.error == nil {
                completion(true,response.data)
            } else {
                completion(false, nil)
                debugPrint(response.error as Any)
            }
        })
        
    }
    
    func changeCurrency(firstCurrency:String, secondCurrency:String, base:String, completion: @escaping CompletionHandler) {
        
        let symbols = "\(firstCurrency),\(secondCurrency)"
        
        let parameters = ["symbols": symbols, "base": base]
        
        AF.request("\(BASE_URL)\(ALL_CURRENCY)", method: .get, parameters: parameters).responseJSON(completionHandler: { (response) in
            if response.error == nil {
                completion(true,response.data)
            } else {
                completion(false, nil)
                debugPrint(response.error as Any)
            }
        })
        
        //https://api.exchangeratesapi.io/latest?symbols=USD,GBP&base=USD
    }
    
}
