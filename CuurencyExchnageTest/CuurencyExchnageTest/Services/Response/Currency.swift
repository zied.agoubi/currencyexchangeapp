//
//  Currency.swift
//  CuurencyExchnageTest
//
//  Created by MacBook Pro Catalina on 08/03/2020.
//  Copyright © 2020 MacBook Pro Catalina. All rights reserved.
//

import Foundation

class Currency: Codable {
    
    var rates: [String: Double]?
    let base: String?
    let date: String?
    
    enum CodingKeys: String, CodingKey {
        case rates = "rates"
        case base = "base"
        case date = "date"
    }
    
}

