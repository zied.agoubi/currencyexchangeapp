//
//  HistoryExchange.swift
//  CuurencyExchnageTest
//
//  Created by MacBook Pro Catalina on 08/03/2020.
//  Copyright © 2020 MacBook Pro Catalina. All rights reserved.
//

import Foundation

struct HistoryExchange: Codable {
    
    var usedCurrency: String?
    var demandedCurrency: String?
    var amountValue: String?
    var resultExchange: String?
    var date: String?
    
    init(usedCurrency: String, demandedCurrency: String, amountValue: String, resultExchange: String, date: String) {
        self.usedCurrency = usedCurrency
        self.demandedCurrency = demandedCurrency
        self.amountValue = amountValue
        self.resultExchange = resultExchange
        self.date = date
    }
    
    

    public static func saveHistory(exchnageArray: [HistoryExchange]){
        let placesData = try! JSONEncoder().encode(exchnageArray)
        UserDefaults.standard.set(placesData, forKey: "currency")
    }

    public static func getPlaces() -> [HistoryExchange]?{
        guard let placeData = UserDefaults.standard.data(forKey: "currency") else {return []}
        let placeArray = try! JSONDecoder().decode([HistoryExchange].self, from: placeData)
        return placeArray
    }
    
}
