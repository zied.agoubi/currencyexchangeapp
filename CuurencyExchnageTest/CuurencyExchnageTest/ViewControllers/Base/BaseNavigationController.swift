//
//  BaseNavigationController.swift
//  CuurencyExchnageTest
//
//  Created by MacBook Pro Catalina on 07/03/2020.
//  Copyright © 2020 MacBook Pro Catalina. All rights reserved.
//

import Foundation
import UIKit

class BaseNavigationController: UINavigationController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            if let presentedViewController = self.presentedViewController, !presentedViewController.isBeingDismissed {
                return presentedViewController.preferredStatusBarStyle
            } else {
                return self.topViewController?.preferredStatusBarStyle ?? .lightContent
            }
        }
    }
}
