//
//  BaseViewController.swift
//  CuurencyExchnageTest
//
//  Created by MacBook Pro Catalina on 07/03/2020.
//  Copyright © 2020 MacBook Pro Catalina. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController: UIViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
                
        self.navigationItem.hidesBackButton = true
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        if let navigationController = self.navigationController, navigationController.viewControllers.count > 1 {
            
            let menuBtn = UIButton(type: .custom)
            menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 25, height: 25)
            menuBtn.setImage(.icBack, for: .normal)
            menuBtn.addTarget(self, action: #selector(goBack), for: .touchUpInside)

            let menuBarItem = UIBarButtonItem(customView: menuBtn)
            let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 30)
            currWidth?.isActive = true
            let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 30)
            currHeight?.isActive = true
            self.navigationItem.leftBarButtonItem = menuBarItem
            
            weak var weakNav = self.navigationController
            self.navigationController?.interactivePopGestureRecognizer?.delegate = weakNav as? UIGestureRecognizerDelegate
        }
        
    }
    
    func hideLeftButton() {
        self.navigationItem.leftBarButtonItem = nil
    }
    
    @objc func goBack() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func navigatioBar(hide: Bool) {
        self.navigationController?.setNavigationBarHidden(hide, animated: true);
    }
    
    func statusBar(light: Bool) {
        UIApplication.shared.statusBarStyle = light ? .lightContent : .default
    }
    
    func clearNavigationBar () {
        if let navigationController = self.navigationController {
            removeLine(navigationController: navigationController)
            navigationController.navigationBar.isTranslucent = true
            navigationController.view.backgroundColor = .clear
            navigationController.navigationBar.tintColor = UIColor.white
        }
    }
    
    func resetNavigationBar()  {
        self.navigatioBar(hide: false)
        if let navigationController = self.navigationController {
            removeLine(navigationController: navigationController)
            navigationController.navigationBar.isTranslucent = false
            navigationController.navigationBar.tintColor = #colorLiteral(red: 1, green: 0.8189231753, blue: 0.5213443041, alpha: 1)
        }
    }
    
    func removeLine(navigationController: UINavigationController) {
        navigationController.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController.navigationBar.shadowImage = UIImage()
    }
}
