//
//  TableViewCell.swift
//  CuurencyExchnageTest
//
//  Created by MacBook Pro Catalina on 08/03/2020.
//  Copyright © 2020 MacBook Pro Catalina. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    //MARK: - Private properties
    
    //MARK: - Outlets
    @IBOutlet weak var firstTitle: UILabel!
    @IBOutlet weak var secondTitle: UILabel!
        
    //MARK: - LifeCycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    //MARK: - Functions
    
    func configure(history: HistoryExchange) {
        firstTitle.text = "\(history.amountValue ?? "") \(history.usedCurrency ?? "") = \(history.resultExchange ?? "") \(history.demandedCurrency ?? "")"
        secondTitle.text = history.date
    }
    
}
