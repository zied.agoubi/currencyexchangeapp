//
//  HistoryViewController.swift
//  CuurencyExchnageTest
//
//  Created by MacBook Pro Catalina on 07/03/2020.
//  Copyright © 2020 MacBook Pro Catalina. All rights reserved.
//

import UIKit
import RxSwift

class HistoryViewController: BaseViewController {
    
    //MARK: - Private properties
    
    private let viewModel: HistoryViewModel
    private let disposeBag = DisposeBag()
    
    //MARK: - Delegates
    
    //MARK: - Outlets
    
    @IBOutlet weak var historyTableView: UITableView! {
        didSet {
            historyTableView.delegate = self
            historyTableView.dataSource = self
        }
    }
    
    //MARK: - Initializers
    
    init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, viewModel: HistoryViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
         historyTableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "TableViewCell")
        title = "Histórico"
        bindObservables()
        viewModel.viewDidLoad()
    }
    
    private func bindObservables() {
        viewModel.reloadDataObservable
        .subscribe(onNext: { [unowned self] in self.handleReloadDataTableView() })
        .disposed(by: disposeBag)
    }

    //MARK: - HANDLERS
    private func handleReloadDataTableView() {
        DispatchQueue.main.async {
            self.historyTableView.reloadData()
        }
    }
    
}

extension HistoryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.arraysOfHistories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if viewModel.arraysOfHistories.count > 0 {
            guard let cell  = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as? TableViewCell else {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            print(viewModel.arraysOfHistories[indexPath.row])
            cell.configure(history: viewModel.arraysOfHistories[indexPath.row])
            return cell
        } else {
            let cell = UITableViewCell()
            cell.textLabel?.text = "no history found"
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 82
    }
    
}
