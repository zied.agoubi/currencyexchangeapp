//
//  HistoryViewModel.swift
//  CuurencyExchnageTest
//
//  Created by MacBook Pro Catalina on 07/03/2020.
//  Copyright © 2020 MacBook Pro Catalina. All rights reserved.
//

import Foundation
import RxSwift

class HistoryViewModel {
    
    //MARK: - Private Proprties
    
    private let disposeBag = DisposeBag()
    let reloadDataObservable = PublishSubject<Void>()
    var arraysOfHistories: [HistoryExchange] = []
    
    //MARK: - INITIALIZERS
    init() {

    }
    
    func viewDidLoad() {
        getAllHistory()
    }
    
    //MARK: - Private functions
    
    func getAllHistory(){
        arraysOfHistories.removeAll()
        
        guard let array = HistoryExchange.getPlaces() else { return }
        
        arraysOfHistories.append(contentsOf: array)
        
        self.reloadDataObservable.onNext(())
    }
    
}
