//
//  HomeViewController.swift
//  CuurencyExchnageTest
//
//  Created by MacBook Pro Catalina on 07/03/2020.
//  Copyright © 2020 MacBook Pro Catalina. All rights reserved.
//

import UIKit
import RxSwift

protocol HomeViewControllerDelegate: class {
    func didTapHistory(homevc :HomeViewController)
}

class HomeViewController: BaseViewController, UITextFieldDelegate {

    //MARK: - Constants
    
    struct Metrics {
        
    }
    
    //MARK: - Private properties
    
    private let viewModel: HomeViewModel
    private let disposeBag = DisposeBag()
    
    var currencies:[String] = []
    var selectedTag = 0
    
    //MARK: - Delegates
    
    weak var delegate: HomeViewControllerDelegate?
    
    //MARK: - Outlets
    lazy var pickerViewCurrency: UIPickerView = {
        let pickerViewCurrency = UIPickerView()
        pickerViewCurrency.delegate = self
        pickerViewCurrency.dataSource = self
        return pickerViewCurrency
    }()
    
    
    @IBOutlet weak var firstCurrency: CETextfield! {
        didSet {
            firstCurrency.addRightButtonToTextField(width: 35, height: 35, image: .icArrowDown)
        }
    }
    
    @IBOutlet weak var valueOfMoney: CETextfield!
    
    @IBOutlet weak var secondCurrency: CETextfield! {
        didSet {
            secondCurrency.addRightButtonToTextField(width: 35, height: 35, image: .icArrowDown)
        }
    }
    
    @IBOutlet weak var resultOfExchange: CETextfield!
    
    //MARK: - Initializers
    
     init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, viewModel: HomeViewModel) {
           self.viewModel = viewModel
           super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
       
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - LifeCycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideLeftButton()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigatioBar(hide: false)
        self.navigationController?.navigationBar.barTintColor = .lightYellow
        self.title = "Conversão de Moeda"
        firstCurrency.delegate = self
        secondCurrency.delegate = self
        bindObservables()
        viewModel.viewDidLoad()
        
        let history = UIBarButtonItem(title: "Histórico", style: .plain, target: self, action: #selector(historyTapped))
        history.tintColor = .black
        self.navigationItem.rightBarButtonItems = [history]
    }
    
    //MARK: - Bind Observables
    private func bindObservables() {
        viewModel.reloadDataObservable
        .subscribe(onNext: { [unowned self] in self.handleReloadDataTableView() })
        .disposed(by: disposeBag)
        
        viewModel.loadingObservable
        .subscribe(onNext: { [weak self] in self?.handleLoading($0) })
        .disposed(by: disposeBag)
        
        viewModel.reloadConvertCurrencyObservable
        .subscribe(onNext: { [unowned self] in self.handleReloadConvertCurrency() })
        .disposed(by: disposeBag)
    }

    //MARK: - HANDLERS
    private func handleReloadDataTableView() {
        DispatchQueue.main.async {
            guard let rates = self.viewModel.listOfCurrencies?.rates else { return }
            debugPrint(rates)
            self.currencies.removeAll()
            for item in rates {
                self.currencies.append(item.key)
            }
            self.pickerViewCurrency.reloadAllComponents()
        }
    }
    
    private func handleLoading(_ loading: Bool) {
        DispatchQueue.main.async {
            if loading {
                self.showSpinner(onView: self.view)
            } else {
                self.removeSpinner()
            }
        }
    }
    
    private func handleReloadConvertCurrency() {
        guard let result = viewModel.resultConvertOperation else {
            return
        }
        
        DispatchQueue.main.async {
            self.resultOfExchange.text = String(format: "%.2f", result)
            
            self.viewModel.saveExchanges(usedCurrency: self.firstCurrency.text!, demandedCurrency: self.secondCurrency.text!, amountValue: self.valueOfMoney.text!, resultExchange: self.resultOfExchange.text!)
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 0 || textField.tag == 2  {
           textField.inputView = pickerViewCurrency
            selectedTag = textField.tag
            pickerViewCurrency.tag = textField.tag
        }
        
        return true
    }
    
    //MARK: - Functions
    
    @IBAction func convertAction(_ sender: CEButton) {
        if firstCurrency.text!.isEmpty || valueOfMoney.text!.isEmpty || secondCurrency.text!.isEmpty {
            self.showAlert(alertText: "error", alertMessage: "you need to enter the neccessary data for the currency exchange")
        } else {
            let askedAmount = (valueOfMoney.text! as NSString).doubleValue
            viewModel.handleConvertedCurrency(firstCurrency: firstCurrency.text!, secondCurrency: secondCurrency.text!, base: firstCurrency.text!, askedAmount: askedAmount)
        }
        
    }
    
    @objc func historyTapped() {
        delegate?.didTapHistory(homevc: self)
    }
    
}

extension HomeViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return currencies.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return currencies[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if selectedTag == 0 {
            firstCurrency.text = currencies[row]
        } else {
            secondCurrency.text = currencies[row]
        }
    }
    
}

