//
//  HomeViewModel.swift
//  CuurencyExchnageTest
//
//  Created by MacBook Pro Catalina on 07/03/2020.
//  Copyright © 2020 MacBook Pro Catalina. All rights reserved.
//

import Foundation
import RxSwift

class HomeViewModel {
    
    //MARK: - Private Proprties
    
    private let disposeBag = DisposeBag()
    let reloadDataObservable = PublishSubject<Void>()
    let loadingObservable = PublishSubject<Bool>()
    var listOfCurrencies: Currency?
    
    let reloadConvertCurrencyObservable = PublishSubject<Void>()
    var convertedValue: Currency?
    
    var resultConvertOperation: Double?
    
    //MARK: - Private Proprties
    
    
    //MARK: - INITIALIZERS
    init() {
        
    }
    
    func viewDidLoad() {
        handleGetAllCurrencies()
    }
    
    //MARK: - Private functions
    
    func handleGetAllCurrencies(){
        loadingObservable.onNext(true)
        ApiRequest.instance.getCurrency { (success, data) in
            if success {
                self.listOfCurrencies?.rates?.removeAll()
                DispatchQueue.global(qos: .userInitiated).async {
                    guard let unwrapedData = data else {return}
                    guard let jsonString = String(data: unwrapedData, encoding: .utf8) else {return}
                    guard let jsonData = jsonString.data(using: .utf8) else { return }
                    
                    do {
                        let cr = try JSONDecoder().decode(Currency.self, from: jsonData)
                        self.listOfCurrencies = cr
                        self.loadingObservable.onNext(false)
                        self.reloadDataObservable.onNext(())
                    } catch let error as NSError {
                        print("json parse error: \(error.localizedDescription)")
                        self.loadingObservable.onNext(false)
                    }
                }
            } else {
                print("Network service error")
                self.loadingObservable.onNext(false)
            }
        }

    }
    
    func handleConvertedCurrency(firstCurrency:String, secondCurrency:String, base:String, askedAmount: Double) {
        loadingObservable.onNext(true)
        ApiRequest.instance.changeCurrency(firstCurrency: firstCurrency, secondCurrency: secondCurrency, base: base) { (success, data) in
            if success {
                self.convertedValue?.rates?.removeAll()
                DispatchQueue.global(qos: .userInitiated).async {
                    guard let unwrapedData = data else {return}
                    guard let jsonString = String(data: unwrapedData, encoding: .utf8) else {return}
                    guard let jsonData = jsonString.data(using: .utf8) else { return }
                    
                    do {
                        let convertedCr = try JSONDecoder().decode(Currency.self, from: jsonData)
                        self.convertedValue = convertedCr
                        self.handleConvertOperation(askedAmount: askedAmount, base: base)
                        self.loadingObservable.onNext(false)
                    } catch let error as NSError {
                        print("json parse error: \(error.localizedDescription)")
                        self.loadingObservable.onNext(false)
                    }
                }
            } else {
                print("Network service error")
                self.loadingObservable.onNext(false)
            }
        }
    }
    
    func handleConvertOperation(askedAmount: Double, base: String) {
        guard let rates = convertedValue?.rates else { return }
        
        if rates.count > 0 {
            print("its fine")
            for item in rates {
                if item.key != base {
                    resultConvertOperation = item.value * askedAmount
                    self.reloadConvertCurrencyObservable.onNext(())
                }
            }
        }
    }
    
    func saveExchanges(usedCurrency: String, demandedCurrency: String, amountValue:String, resultExchange: String) {
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        let currentDate = formatter.string(from: date)
        
        let history = HistoryExchange(usedCurrency: usedCurrency, demandedCurrency: demandedCurrency, amountValue: amountValue, resultExchange: resultExchange, date: currentDate)
        
        var historyArray = [HistoryExchange]()
        historyArray.removeAll()
        guard let permanantHistoryArray = HistoryExchange.getPlaces() else {return}
        historyArray.append(contentsOf: permanantHistoryArray)
        historyArray.append(history)
        HistoryExchange.saveHistory(exchnageArray: historyArray)
    }
    
    
    
}
