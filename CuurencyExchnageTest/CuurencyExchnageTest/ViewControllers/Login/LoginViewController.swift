//
//  LoginViewController.swift
//  CuurencyExchnageTest
//
//  Created by MacBook Pro Catalina on 07/03/2020.
//  Copyright © 2020 MacBook Pro Catalina. All rights reserved.
//

import UIKit
import RxSwift

protocol LoginViewControllerDelegate: class {
    func didSuccesfullLogin()
}

class LoginViewController: BaseViewController {
    
    //MARK: - Private properties
    
    private let viewModel: LoginViewModel
    private let disposeBag = DisposeBag()
    
    //MARK: - Delegates
    
    weak var delegate: LoginViewControllerDelegate?
    
    //MARK: - Outlets
    
    @IBOutlet weak var txtUsername: CETextfield!
    
    @IBOutlet weak var txtPassword: CETextfield! {
        didSet {
            txtPassword.isSecureTextEntry = true
            txtPassword.textContentType = .password
        }
    }
    
    //MARK: - Initializers
    
     init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, viewModel: LoginViewModel) {
           self.viewModel = viewModel
           super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
       
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigatioBar(hide: false)
        self.navigationController?.navigationBar.barTintColor = .lightYellow
        self.title = "Login to your app"
    }
    
    //MARK: - Functions
    
    @IBAction func loginAction(_ sender: CEButton) {
        if txtUsername.text!.isEmpty || txtPassword.text!.isEmpty{
            self.showAlert(alertText: "Error", alertMessage: "You need to enter your email and your password")
        } else if txtUsername.text != "zied" && txtPassword.text != "123456" {
            self.showAlert(alertText: "Error", alertMessage: "Wrong username or password")
        } else {
            delegate?.didSuccesfullLogin()
        }
    }


}
